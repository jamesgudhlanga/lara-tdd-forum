<?php

namespace App\Models\Threads;

use App\Models\Activities\Activity;
use App\Models\Activities\RecordsActivity;
use App\Models\Channels\Channel;
use App\Models\Replies\Reply;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use RecordsActivity;

    protected $guarded =[];

    protected $with = ['creator', 'channel'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('replyCount', function ($builder) {
            $builder->withCount('replies');
        });


        static::deleting(function($thread){
            $thread->replies->each->delete();
        });
    }

    public function path()
    {
        return "/threads/{$this->channel->slug}/{$this->id}";
    }

    public function replies()
    {
        return $this->hasMany(Reply::class, 'thread_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function addReply($reply)
    {
        $this->replies()->create($reply);
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class, 'channel_id');
    }
    
    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

}
