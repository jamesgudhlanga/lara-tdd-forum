<?php

namespace App\Models\Replies;


trait Favouritable
{

    public function favourite()
    {
        $attributes = ['user_id' => auth()->id()];
        if (!$this->favourites()->where($attributes)->exists()) {
            $this->favourites()->create($attributes);
        }
    }

    public function unfavourite()
    {
        $attributes = ['user_id' => auth()->id()];
        $this->favourites()->where($attributes)->get()->each->delete();
    }

    public function isFavourated()
    {
        return !!$this->favourites->where('user_id', auth()->id())->count();
    }

    public function getFavouritesCountAttribute()
    {
        return $this->favourites->count();
    }

    public function getIsFavouratedAttribute()
    {
        return $this->isFavourated();
    }
}
