<?php

namespace App\Models\Replies;

use App\Models\Activities\RecordsActivity;
use App\Models\Threads\Thread;
use App\Models\Users\User;
use App\Models\Favourites\Favourite;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use RecordsActivity, Favouritable;

    protected $guarded = [];

    protected $with = ['owner', 'favourites'];

    protected $appends = ['favouritesCount', 'isFavourated'];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    public function favourites()
    {
        return $this->morphMany(Favourite::Class, 'favourited');
    }

    public function path()
    {
        return $this->thread->path()."#reply-{$this->id}";
    }
}
