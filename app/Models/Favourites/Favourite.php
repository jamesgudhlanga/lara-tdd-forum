<?php

namespace App\Models\Favourites;

use App\Models\Activities\RecordsActivity;
use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    use RecordsActivity;

    public $guarded = [];

    public function favourited()
    {
        return $this->morphTo();
    }
}
