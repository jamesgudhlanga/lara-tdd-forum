<?php

namespace App\Models\Channels;

use App\Models\Threads\Thread;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    function getRouteKeyName()
    {
        return 'slug';
    }

    public function threads()
    {
        return $this->hasMany(Thread::class);
    }
}
