<?php

namespace App\Filters\Threads;

use App\Models\Users\User;
use Illuminate\Http\Request;

/**
 * Class ThreadFilters
 * @package App\Filters\Threads
 */
class ThreadFilters extends Filters
{
    protected $filters = ['by', 'popular'];

    /**
     * Filter threads by username
     * @param $username
     * @return mixed
     */
    protected function by($username)
    {
        $user = User::where('name', $username)->firstOrFail();
        return $this->builder->where('user_id', $user->id);
    }

    /**
     * Filter threads by popularity
     * @return mixed
     */
    protected function popular()
    {
        $this->builder->getQuery()->orders =[];
        return $this->builder->orderBy('replies_count', 'desc');
    }
}
