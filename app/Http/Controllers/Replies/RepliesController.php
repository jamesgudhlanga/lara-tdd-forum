<?php

namespace App\Http\Controllers\Replies;

use App\Models\Replies\Reply;
use App\Models\Threads\Thread;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

   
    public function index()
    {
        //
    }

    
    public function create()
    {
        //
    }


    public function store($channelId, Thread $thread)
    {
        $this->validate(request(), ['body' => 'required']);

        $thread->addReply([
           'body' => request('body'),
           'user_id' => auth()->id()
        ]);

        return back()->with('flash', 'Your thread response successfully saved');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    
    public function update(Reply $reply)
    {
        $this->authorize('update', $reply);

        $reply->update(request(['body']));
    }

   
    public function destroy(Reply $reply)
    {
        $this->authorize('update', $reply);

        $reply->delete();

        $message = 'The reply record successfully deleted';
        if(request()->expectsJson()){
            return response(['status' => $message]);
        }

        return back()->with('flash', $message);
    }

}
