<?php

namespace App\Http\Controllers\Threads;

use App\Filters\Threads\ThreadFilters;
use App\Models\Channels\Channel;
use App\Models\Threads\Thread;
use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ThreadsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index(Channel $channel, ThreadFilters $filters)
    {
        $threads = Thread::latest()->filter($filters);
        if ($channel->exists)  {
            $threads = $channel->threads()->latest();
        }

        $threads = $threads->get();

        if(request()->wantsJson()) {
            return $threads;
        }

        return view('threads.index', compact('threads'));
    }


    public function create()
    {
        return view('threads.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(),['title' => 'required', 'body' => 'required', 'channel_id' => 'required|exists:channels,id']);

        $thread = Thread::create([
            'user_id' => auth()->id(),
            'channel_id' => request('channel_id'),
            'title' => request('title'),
            'body' => request('body')
        ]);

        return redirect($thread->path())
            ->with('flash', 'Your thread was successfully published');
    }


    public function show($channelID, Thread $thread)
    {
        $replies = $thread->replies()->paginate(10);
        return view('threads.show', compact('thread', 'replies'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($channel, Thread $thread)
    {
        $this->authorize('update', $thread);


        $thread->delete();

        if(request()->wantsJson()) {
            return response([], 204);
        }

        return redirect("/threads");

    }
}
