<?php

namespace App\Http\Controllers\Profiles;

use App\Models\Activities\Activity;
use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfilesController extends Controller
{
    public function show(User $user)
    {
        $profileUser = $user;
        $activities = Activity::feed($user);

        return view('profiles.show', compact('profileUser', 'activities'));
    }
}
