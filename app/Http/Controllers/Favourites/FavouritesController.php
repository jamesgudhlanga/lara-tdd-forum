<?php

namespace App\Http\Controllers\Favourites;

use App\Models\Favourites\Favourite;
use App\Models\Replies\Reply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FavouritesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Reply $reply)
    {
        $reply->favourite();
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Reply $reply)
    {
        $reply->unfavourite();
    }
}
