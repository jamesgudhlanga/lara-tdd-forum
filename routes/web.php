<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('threads/create', 'Threads\ThreadsController@create')->name('threads.create');
Route::get('threads', 'Threads\ThreadsController@index')->name('threads.index');
Route::get('threads/{channel}', 'Threads\ThreadsController@index');
Route::get('threads/{channel}/{thread}', 'Threads\ThreadsController@show')->name('threads.show');
Route::delete('threads/{channel}/{thread}', 'Threads\ThreadsController@destroy')->name('threads.destroy');
Route::post('threads', 'Threads\ThreadsController@store')->name('threads.store');

Route::prefix('threads/{channel}/{thread}')->group(function () {
    Route::post('replies', 'Replies\RepliesController@store');
});
Route::delete('replies/{reply}', 'Replies\RepliesController@destroy');
Route::patch('replies/{reply}', 'Replies\RepliesController@update');

Route::post('replies/{reply}/favourites', 'Favourites\FavouritesController@store');
Route::delete('replies/{reply}/favourites', 'Favourites\FavouritesController@destroy');

Route::get('/profiles/{user}', 'Profiles\ProfilesController@show');
