<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Channels\Channel::class, function (Faker $faker) {
    $name = $faker->word;
    return [
        'name' => ucfirst($name),
        'slug' => str_slug($name)
    ];
});
