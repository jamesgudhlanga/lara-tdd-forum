<?php

use Faker\Generator as Faker;
use App\Models\Users\User;
use App\Models\Channels\Channel;

$factory->define(App\Models\Threads\Thread::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return create(User::class)->id;
        },
        'channel_id' => function() {
            return create(Channel::class)->id;
        },
        'title' => $faker->sentence,
        'body' => $faker->paragraph
    ];
});
