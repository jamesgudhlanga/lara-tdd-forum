<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Replies\Reply::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory('App\Models\Users\User')->create()->id;
        },
        'thread_id' => function() {
            return factory('App\Models\Threads\Thread')->create()->id;
        },
        'body' => $faker->paragraph
    ];
});
