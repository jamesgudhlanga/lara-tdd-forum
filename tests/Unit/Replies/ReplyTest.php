<?php

namespace Tests\Unit\Replies;

use App\Models\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReplyTest extends TestCase
{
    use RefreshDatabase;
   /**
    * @test
    */
   public function it_has_an_owner()
   {
       $reply = create('App\Models\Replies\Reply');

       $this->assertInstanceOf(User::class, $reply->owner);
   }
}
