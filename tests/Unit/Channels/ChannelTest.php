<?php

namespace Tests\Unit\Channels;

use App\Models\Channels\Channel;
use App\Models\Threads\Thread;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChannelTest extends TestCase
{
    use RefreshDatabase;

     /** @test */
     public function a_channel_consists_of_threads()
     {
         $channel = create(Channel::class);
         $thread = create(Thread::class, ['channel_id' => $channel->id]);

         $this->assertTrue($channel->threads->contains($thread));
     }
}
