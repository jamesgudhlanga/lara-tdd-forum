<?php

namespace Tests\Unit\Threads;

use App\Models\Threads\Thread;
use App\Models\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ThreadTest extends TestCase
{
    use RefreshDatabase;

    protected $thread;

    public function setUp()
    {
        parent::setUp();
        $this->thread = create(Thread::class);
    }

    /**
     * @test
     */
    public function that_a_thread_has_creator()
    {
        $this->assertInstanceOf(User::class, $this->thread->creator);
    }

    /**
     * @test
     */
    public function a_thread_has_replies()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->thread->replies);
    }


    /**
     * @test
     */
    public function a_thread_can_add_reply()
    {
        $this->thread->addReply([
            'body' =>'Testing',
            'user_id' => 1
        ]);

        $this->assertCount(1, $this->thread->replies);
    }
    
      /** @test */
      public function a_thread_belongs_to_channel()
      {
          $this->assertInstanceOf('App\Models\Channels\Channel', $this->thread->channel);
      }

       /** @test */
       public function a_thread_can_make_a_string_a_path()
       {
            $this->assertEquals("/threads/{$this->thread->channel->slug}/{$this->thread->id}", $this->thread->path());
       }
}
