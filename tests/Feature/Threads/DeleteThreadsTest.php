<?php

namespace Tests\Feature\Threads;

use App\Models\Activities\Activity;
use App\Models\Replies\Reply;
use App\Models\Threads\Thread;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteThreadsTest extends TestCase
{
    use RefreshDatabase;

     /** @test */
     public function unauthorized_users_can_not_delete_a_thread()
     {
        $thread = create(Thread::class);
        $this->withExceptionHandling()
            ->delete($thread->path())
            ->assertRedirect('login');

        //A signed user should not delete if they are not authorized to do so
         $this->signIn()
             ->withExceptionHandling()
             ->delete($thread->path())
             ->assertStatus(403);
     }

     /** @test */
     public function authorized_user_can_delete_a_thread()
     {
         $this->signIn();
         $thread = create(Thread::class, ['user_id' => auth()->id()]);

         $reply = create(Reply::class, ['thread_id' => $thread->id]);

         $this->json('DELETE', $thread->path())
            ->assertStatus(204);

         /* Assert That all the activities are deleted */
         $this->assertDatabaseMissing('threads', ['id' => $thread->id]);
         $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
         $this->assertEquals(0, Activity::count());
     }
}
