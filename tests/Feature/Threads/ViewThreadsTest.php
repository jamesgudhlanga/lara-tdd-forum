<?php

namespace Tests\Feature\Threads;

use App\Models\Channels\Channel;
use App\Models\Replies\Reply;
use App\Models\Threads\Thread;
use App\Models\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewThreadsTest extends TestCase
{
    use RefreshDatabase;
    protected $thread;

    public function setUp()
    {
        parent::setUp();
        $this->thread = create(Thread::class);
    }

    /**
     * @test
     */
    public function can_view_all_threads()
    {
        $this->get('/threads')
            ->assertSee($this->thread->title);
    }

    /**
     * @test
     */
    public function can_view_a_single_thread()
    {
        $this->get($this->thread->path())
            ->assertSee($this->thread->title);
    }

    /**
     * @test
     */
    public function can_view_replies_associated_to_a_thread()
    {
        $reply = create('App\Models\Replies\Reply', ['thread_id' => $this->thread->id]);
        $this->get($this->thread->path())
            ->assertSee($reply->body);
    }

     /** @test */
     public function it_filter_threads_by_channel()
     {
         $channel = create(Channel::class);
         $channel2 = create(Channel::class);
         $threadInChannel = create(Thread::class, ['channel_id' => $channel->id]);
         $threadNotInChannel = create(Thread::class,['channel_id' => $channel2->id]);

         $this->get("/threads/{$channel->slug}")
            ->assertSee($threadInChannel->body)
            ->assertDontSee($threadNotInChannel->body);
     }
      /** @test */
      public function a_user_can_filter_threads_by_a_username()
      {
          $this->signIn(create(User::class, ['name' => 'James Gudhlanga']));

          $threadByJames = create(Thread::class, ['user_id' => auth()->id()]);
          $threadNotByJames = create(Thread::class);

          $this->get("/threads?by=James Gudhlanga")
              ->assertSee($threadByJames->title)
              ->assertDontSee($threadNotByJames->title);
      }


       /** @test */
       public function a_user_can_filter_threads_by_popularity()
       {
            $threadWithNoReplies = $this->thread;
            $threadWithTwoReplies = create(Thread::class);
            create(Reply::class, ['thread_id' => $threadWithTwoReplies->id], 2);
            $threadWithThreeReplies = create(Thread::class);
            create(Reply::class, ['thread_id' => $threadWithThreeReplies->id], 3);

            $res = $this->getJson('/threads?popular=1')->json();

            $this->assertEquals([3,2,0], array_column($res, 'replies_count'));
       }

}
