<?php

namespace Tests\Feature\Threads;

use App\Models\Channels\Channel;
use App\Models\Threads\Thread;
use App\Models\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateThreadTest extends TestCase
{
    use RefreshDatabase;

    protected $user;
    protected $thread;

    public function setUp()
    {
        parent::setUp();
        $this->user = create(User::class);
        $this->thread = make(Thread::class);
    }

    /**
     * @test
     */
    public function a_guest_can_not_create_a_thread()
    {
        $this->withExceptionHandling()
            ->get(route('threads.create'))
            ->assertRedirect(route('login'));

        $this->withExceptionHandling()
            ->post(route('threads.store'), [])
            ->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_create_a_new_thread()
    {
        $this->signIn($this->user);

        $res = $this->post('/threads', $this->thread->toArray())
            ->assertSessionHas('flash');

        $this->get($res->headers->get('Location'))
            ->assertSee($this->thread->title)
            ->assertSee($this->thread->body);
    }

    /**
     * @test
     */
    public function an_authenticated_user_can_see_a_create_form()
    {
        $this->signIn();
        $this->get(route('threads.create'))
            ->assertViewIs('threads.create')
            ->assertViewHas('channels')
            ->assertStatus(200);
    }

    /** @test */
    public function a_title_is_required_to_store_a_thread()
    {
        $this->publishThread(['title' => null])
            ->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_body_is_required_to_store_thread()
    {
        $this->publishThread(['body' => null])
            ->assertSessionHasErrors('body');
    }

    /** @test */
    public function a_valid_channel_is_required_to_store_thread()
    {
        create(Channel::class,[], 2);

        $this->publishThread(['channel_id' => 999])
            ->assertSessionHasErrors('channel_id');
    }

    protected function publishThread($overrides = [])
    {
        $thread = make(Thread::class, $overrides);
        return $this->signIn()
            ->withExceptionHandling()
            ->post('/threads', $thread->toArray());
    }

}
