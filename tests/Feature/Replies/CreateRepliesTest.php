<?php

namespace Tests\Feature\Replies;

use App\Models\Replies\Reply;
use App\Models\Threads\Thread;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateRepliesTest extends TestCase
{
    use RefreshDatabase;

    protected $thread;
    protected $reply;

    public function setUp()
    {
        parent::setUp();
        $this->thread = create(Thread::class);
        $this->reply = make(Reply::class,['thread_id' => $this->thread->id]);
    }

    /**
     * @test
     */
    public function guest_user_may_not_add_replies()
    {
        $this->withExceptionHandling()
            ->post($this->thread->path().'/replies', [])
            ->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function an_authenticated_user_may_participate_in_forum_threads()
    {
        $this->signIn();

        $this->post($this->thread->path().'/replies', $this->reply->toArray())
            ->assertSessionHas('flash');

        $this->get($this->thread->path())
            ->assertSee($this->reply->body);
    }

     /** @test */
     public function a_body_is_required_to_create_a_reply()
     {
        $this->reply->body = null;
        $this->signIn()->withExceptionHandling()->post($this->thread->path().'/replies', $this->reply->toArray())
            ->assertSessionHasErrors('body');
     }
}
