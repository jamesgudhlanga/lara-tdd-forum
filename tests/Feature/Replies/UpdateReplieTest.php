<?php

namespace Tests\Feature\Replies;

use App\Models\Replies\Reply;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateReplieTest extends TestCase
{
   use RefreshDatabase;

    /** @test */
    public function unauthorised_user_can_not_update_replies()
    {
        $reply = create(Reply::class);
        $this->withExceptionHandling()
            ->patch("/replies/{$reply->id}")
            ->assertRedirect('login');

        //you delete what u have created
        $this->signIn()
            ->patch("/replies/{$reply->id}")
            ->assertStatus(403);
    }
    /** @test */
    public function an_authorized_user_can_update_a_reply()
    {
        $this->signIn();
        $reply = create(Reply::class,['user_id' => auth()->id()]);
        $updatedReply = 'Time for change';

        $this->patch("/replies/{$reply->id}", ['body' => $updatedReply]);

        $this->assertDatabaseHas('replies', ['id' => $reply->id, 'body' => $updatedReply]);
    }
}
