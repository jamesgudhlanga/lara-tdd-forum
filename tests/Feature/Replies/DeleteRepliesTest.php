<?php

namespace Tests\Feature\Replies;

use App\Models\Replies\Reply;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteRepliesTest extends TestCase
{
   use RefreshDatabase;
    /** @test */
    public function unauthorised_user_can_not_delete_replies()
    {
        $reply = create(Reply::class);
        $this->withExceptionHandling()
            ->delete("/replies/{$reply->id}")
            ->assertRedirect('login');

        //you delete what u have created
        $this->signIn()
            ->delete("/replies/{$reply->id}")
            ->assertStatus(403);
    }

     /** @test */
     public function an_authorised_user_can_delete_a_reply()
     {
         $this->signIn();
         $reply = create(Reply::class, ['user_id' => auth()->id()]);

         $this->delete("/replies/{$reply->id}")
              ->assertStatus(302)
              ->assertSessionHas('flash');
         $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
     }

}
