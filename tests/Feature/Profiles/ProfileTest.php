<?php

namespace Tests\Feature\Profiles;

use App\Models\Activities\Activity;
use App\Models\Threads\Thread;
use App\Models\Users\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;
     /** @test */
     public function a_user_has_a_profile()
     {
         $user = create(User::class);

         $this->get("/profiles/{$user->name}")
             ->assertSee($user->name);
     }

      /** @test */
      public function a_profile_displays_all_threads_created_by_a_user()
      {
          $this->signIn();
          $thread = create(Thread::class, ['user_id' =>auth()->id()]);
          $this->get("/profiles/".auth()->user()->name)
              ->assertSee($thread->title)
              ->assertSee($thread->body);
      }
}
