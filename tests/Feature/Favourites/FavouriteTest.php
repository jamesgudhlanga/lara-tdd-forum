<?php

namespace Tests\Feature\Favourites;

use App\Models\Replies\Reply;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FavouriteTest extends TestCase
{
    use RefreshDatabase;

    protected $reply;

    public function setUp()
    {
        parent::setUp();
        $this->reply = create(Reply::class);
    }

    /** @test */
     public function a_guest_can_not_favourite_anything()
     {
         $this->withExceptionHandling()
             ->post("/replies/{$this->reply->id}/favourites")
             ->assertRedirect('/login');
     }

     /** @test */
     public function an_authenticated_user_can_favourite_a_reply()
     {
        $this->signIn()
            ->post("/replies/{$this->reply->id}/favourites");

        $this->assertCount(1, $this->reply->favourites);
     }

      /** @test */
      public function an_authenticated_user_may_only_favourite_a_reply_once()
      {
          $this->signIn();

          try{
              $this->post("/replies/{$this->reply->id}/favourites");
              $this->post("/replies/{$this->reply->id}/favourites");
          }catch(\Exception $e)
          {
              $this->fail($e->getMessage());
          }
          $this->assertCount(1, $this->reply->favourites);
      }

    /** @test */
    public function an_authenticated_user_can_un_favourite_a_reply()
    {
        $this->signIn();
        $this->reply->favourite();

        $this->delete("/replies/{$this->reply->id}/favourites");
        $this->assertCount(0, $this->reply->fresh()->favourites);
    }
}
