@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Thread</div>

                    <div class="card-body">
                        <form action="{{ route('threads.store') }}" method="POST">
                            @include('threads._partials.forms.thread')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
