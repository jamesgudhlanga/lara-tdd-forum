<reply :attributes="{{ $reply }}" inline-template v-cloak>
    <div id="reply-{{$reply->id}}" class="card my-2">
        <div class="card-header">
            <div class="level">
                <h5 class="flex">
                    <a href="/profiles/{{$reply->owner->name}}">{{ $reply->owner->name }}</a> said {{ $reply->created_at->diffForHumans() }}..
                </h5>
                <div>
                    <favourite :reply="{{ $reply }}"></favourite>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div v-if="editing">
                <div class="form-group">
                    <textarea  class="form-control" v-model="body"></textarea>
                   <div class="mt-2">
                       <button class="btn btn-success btn-sm" @click="update()">Update</button>
                       <button class="btn btn-secondary btn-sm" @click="editing=false">Cancel</button>
                   </div>
                </div>
            </div>
            <div v-else v-text="body"></div>
        </div>
        @can('update', $reply)
            <div class="card-footer level" v-if="editing==false">
                <span class="mr-2">
                    <button class="btn btn-sm btn-outline-primary" @click="editing = true">{{ __('Edit') }}</button>
                </span>
                <span>
                    <button class="btn btn-sm btn-outline-danger" @click="destroy()">{{ __('Delete') }}</button>
                </span>
            </div>
        @endcan
    </div>
</reply>
