@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="text-uppercase font-weight-bold text-secondary">
                <h3>Forum Threads</h3>
            </div>
            @forelse($threads as $thread)
                <div class="card mt-3">
                    <div class="card-body">
                        <article>
                            <div class="level">
                                <h4 class="flex">
                                    <a href="/profiles/{{$thread->creator->name}}">{{ $thread->creator->name }}</a> posted:
                                    <a href="{{ $thread->path() }}">{{ $thread->title }}</a>
                                </h4>
                                <a href="{{ $thread->path() }}">
                                    {{ $thread->replies_count }} {{ str_plural('reply', $thread->replies_count) }}
                                </a>
                            </div>
                            <div class="body">{{ $thread->body }}</div>
                        </article>
                    </div>
                </div>
            @empty
                @component('common.empty-component')
                    @slot('message')
                        {{ __('No Threads yet') }}
                    @endslot
                @endcomponent
            @endforelse
        </div>
    </div>
@endsection
