@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="level">
                            <span class="flex">
                                <h4 class="text-secondary font-weight-bold">
                                    <a href="/profiles/{{$thread->creator->name}}">{{ $thread->creator->name }}</a> posted: {{ $thread->title }}
                                </h4>
                            </span>
                        <span>
                                @can('update', $thread)
                                <form action="{{$thread->path()}}" method="POST">
                                        {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn btn-link text-danger">{{ __('Delete Thread') }}</button>
                                    </form>
                            @endcan
                            </span>
                    </div>
                </div>
                <div class="card-body">
                    <article>
                        <div class="body">
                            {{ $thread->body }}
                        </div>
                    </article>
                    <div class="card mt-4">
                        <div class="card-header"><h5 class="text-secondary font-weight-bold">Replies</h5></div>
                        <div class="card-body">
                            @forelse($replies as $reply)
                                @include('threads.replies')
                            @empty
                                @component('common.empty-component')
                                    @slot('message')
                                        {{ __('No Replies yet') }}
                                    @endslot
                                @endcomponent
                            @endforelse
                            {{$replies->links()}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-3">
                <div class="card-body">
                    @if(auth()->check())
                        <form action="{{$thread->path().'/replies'}}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group">
                                <textarea class="form-control {{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" rows="2" placeholder="Have something to say..."></textarea>
                                @if ($errors->has('body'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group text-center">
                                <button class="btn btn-success" type="submit">Post</button>
                            </div>
                        </form>
                    @else
                        <div class="text-center">
                            <p class="lead">Please <a href="{{ route('login') }}">SingIn</a> in to participate in the discussion</p>
                        </div>
                    @endif

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    This thread was published {{ $thread->created_at->diffForHumans() }} by <a href="/profiles/{{$thread->creator->name}}">{{ $thread->creator->name }}</a>,
                    and currently has {{ $thread->replies_count }} {{ str_plural('comment', $thread->replies_count) }}.

                </div>
            </div>
        </div>
    </div>
@endsection
