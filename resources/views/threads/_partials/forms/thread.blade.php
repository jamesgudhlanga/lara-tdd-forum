{{ csrf_field() }}
<div class="form group">
    <label for="channel_id">Choose Channel:</label>
    <select name="channel_id" id="channel_id" class="form-control {{ $errors->has('channel_id') ? ' is-invalid' : '' }}" required>
        <option value="">--select--</option>
        @foreach($channels as $channel)
            <option value="{{$channel->id}}" {{ old('channel_id') == $channel->id ? 'selected' : '' }}>
                {{ $channel->name }}
            </option>
        @endforeach
    </select>
    @if ($errors->has('channel_id'))
        <span class="invalid-feedback" role="alert">{{ $errors->first('channel_id') }}</span>
    @endif
</div>
<div class="form group">
    <label for="title">Title:</label>
    <input type="text" name="title" id="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{old('title')}}" required>
    @if ($errors->has('title'))
        <span class="invalid-feedback" role="alert">{{ $errors->first('title') }}</span>
    @endif
</div>
<div class="form group">
    <label for="body">Body:</label>
    <textarea  name="body" id="body" class="form-control {{ $errors->has('body') ? ' is-invalid' : '' }}" rows="8" required>{{old('body')}}</textarea>
    @if ($errors->has('title'))
        <span class="invalid-feedback" role="alert">{{ $errors->first('title') }}</span>
    @endif
</div>
<div class="col text-center mt-3">
    <button class="btn btn-success" type="submit">Publish</button>
</div>
