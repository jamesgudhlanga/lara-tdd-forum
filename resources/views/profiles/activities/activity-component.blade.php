<div class="card mt-3">
    <div class="card-header">
        <div class="level">
            {{ $heading }}
        </div>
    </div>
    <div class="card-body">
        <article>
            <div class="body">
                {{ $body }}
            </div>
        </article>
    </div>
</div>
