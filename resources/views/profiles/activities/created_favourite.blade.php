@component('profiles.activities.activity-component')
    @slot('heading')
        <span class="flex">
            {{ __('Liked a reply: ') }}
            <a href="{{$activity->subject->favourited->path()}}">
                {{ $activity->subject->favourited->body }}
            </a>
        </span>
    @endslot
    @slot('body')
        {{ $activity->subject->favourited->body }}
    @endslot
@endcomponent

