@component('profiles.activities.activity-component')
    @slot('heading')
        <span class="flex">
            {{ __('Replied to a thread: ') }}
            <a href="{{ $activity->subject->thread->path() }}">
                {{ $activity->subject->thread->title }}
            </a>
        </span>
        <span>{{$activity->subject->created_at->diffForHumans()}}</span>
    @endslot
    @slot('body')
        {{ $activity->subject->body }}
    @endslot
@endcomponent

