@component('profiles.activities.activity-component')
    @slot('heading')
        <span class="flex">
            {{ __('Created a thread: ') }}
            <a href="{{ $activity->subject->path() }}">
                {{ $activity->subject->title }}
            </a>
        </span>
        <span>{{$activity->subject->created_at->diffForHumans()}}</span>
    @endslot
    @slot('body')
        {{ $activity->subject->body }}
    @endslot
@endcomponent

