@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col">
            <div class="text-uppercase font-weight-bold text-secondary text-right">
                <h3 class="d-inline">{{ $profileUser->name }} </h3> <small class="d-inline">Since {{$profileUser->created_at->diffForHumans()}}</small>
            </div>
            @forelse($activities as $date => $activityCollection)
                <div class="text-uppercase font-weight-bold text-secondary mt-3">
                    <h4>{{ $date }}</h4>
                </div>
                @foreach($activityCollection as $activity)
                    @if(view()->exists("profiles.activities.$activity->type"))
                        @include("profiles.activities.$activity->type")
                    @endif
                @endforeach
            @empty
                @component('common.empty-component')
                    @slot('message')
                        {{ __('No activities found') }}
                    @endslot
                @endcomponent
            @endforelse
        </div>
        <div class="col-md-12 text-center mt-3">
            {{--{{$threads->links()}}--}}
        </div>
    </div>
@stop
